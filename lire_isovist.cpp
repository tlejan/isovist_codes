#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>
#include <bits/stdc++.h>

#include "vector.h"

#define PI 3.14159265

using namespace std;

typedef float f;


template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;
  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}




//calculating the position of the points
int cal_orientation(vector3d p, vector3d q, vector3d r){
   int val = (q.z - p.z) * (r.x - q.x) -
   (q.x - p.x) * (r.z - q.z);
   if (val == 0) return 0; //collinear
   return (val > 0)? 1: 2; //clock or counterclockwise
}


int main(int argc, char **argv)
{

//STARTING
	
	if (argc != 3){
		cerr << "Usage : ./lire_isovist isovist_file isovists_nb" << endl;}
	
	
//LOADING ISOVIST.OBJ DATA		

	string filemodel = argv[1];
	
	string outfilename = filemodel + "/isovist.dat";
	
	ofstream fichier2(outfilename.c_str());
	
	fichier2 << "Step Perimeter Area Ellipticity Convexity" << endl;
	
	int N = atof(argv[2]);
	
	for(int i =0; i<N; i++){
	
		string filename = filemodel + "/isovist_" + to_string(i) + ".obj";
	
		ifstream fichier(filename.c_str(), ios::in);
	
		vector< vector3d > points;
		vector< vector <int> > liens;
		bool point = true;
	
		if(fichier){
			while (fichier){
				double x, y, z;
				char u;
				fichier >> u;
			
				point = (u == 'v');
			
				if (point){
					fichier >> x >> y >> z;
					vector3d pt;
				
					pt.x = x;
					pt.y = 0;
					pt.z = z;
					points.push_back(pt);
				}
				else{
					fichier >> x >> y;
					vector<int> link;
					link.push_back(x);
					link.push_back(y);
					liens.push_back(link);
				}
			}
			fichier.close();
			
			int n = points.size();
		
			double perim = points[n-1].distance(points[0]);
		
			double aire = points[n-1].x * points[0].z - points[0].x * points[n-1].z;
		
			double longest_line = 0;
		
			vector< vector3d > hull;
		
			for(int i=0; i<n; i++){
				for(int j=0; j<n; j++){
					double line = points[i].distance(points[j]);
					if(line > longest_line){
						longest_line = line;
					}
				}
			}
		
		/////////////////////CONVEX_HULL////////////////////////////
		
			if(n<=3){hull = points;}
			//left_most point
			int l = 0;
			for(int m = 1; m < n; m++){
				if (points[m].x < points[l].x){
					l = m;
				}
			}
			int p = l, q;

				hull.push_back(points[p]);
				q = (p+1)%n;
				for (int m = 0; m < n; m++){
					if(cal_orientation(points[p], points[m], points[q]) == 2){
						q = m;
					}
				}
				p = q;

		
			while(p!=l && hull.size() < n){
				hull.push_back(points[p]);
				q = (p+1)%n;
				for (int m = 0; m < n; m++){
					if(cal_orientation(points[p], points[m], points[q]) == 2){
						q = m;
					}
				}
				p = q;
			};
		
		////////////////////////////////////////////////////////////
		
			double aire_hull = hull[hull.size()-1].x * hull[0].z - hull[0].x * hull[hull.size()-1].z;
		
			for(int k = 0; k < hull.size() - 1; k++){
				aire_hull = aire_hull + hull[k].x * hull[k+1].z - hull[k+1].x * hull[k].z;
			}
			
			for(int k = 0; k < n - 1; k++){
				double line = points[k].distance(points[k+1]);
				perim = perim + line;
				aire = aire + points[k].x * points[k+1].z - points[k+1].x * points[k].z;
			}
		
			fichier2 << i << " " << perim << " " << 0.5*aire << " " << PI * longest_line * longest_line / (4 * 0.5 * aire) << " " << aire/aire_hull << endl;

		}
	
		else {cerr << "impossible d'ouvrir le fichier" << endl;}
	}
	
	cout << "Succesfully saved data from " << N << " isovists to " << outfilename << endl;
	
fichier2.close();
return(0);
}
