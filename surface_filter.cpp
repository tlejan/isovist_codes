#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>

#include "vector.h"

using namespace std;



/////////////////////////MAIN FUNCTION//////////////////////////////////


int main (int argc, char **argv){
	
	if(argc != 3){
		cout << "Usage : ./executable model.obj surface_limit edge_limit" << endl;
	}
	
	string filemodel = argv[1];
	
	double limit_surface = atof(argv[2]);
	
	double limit_edge = atof(argv[3]);
	
	ifstream fichier(filemodel.c_str(), ios::in);
	
	vector< vector3d > points;
	vector< vector <int> > liens;
	bool point = true;
	int i = 0;
	double min_surface = 0;
	double max_surface = 0;
	double min_edge = 0;
	double max_edge = 0;
	
	if(fichier){
		while (fichier){
			double x, y, z;
			char u;
			fichier >> u >> x >> y >> z;
			
			point = (u == 'v');
			
			if (point){
				vector3d pt;
				pt.x = x;
				pt.y = y;
				pt.z = z;
				points.push_back(pt);
			}
			else{
				vector3d u = points[x - 1] - points[y - 1];
				vector3d v = points[x - 1] - points[z - 1];
				vector3d cross = u.cross_product(v);
				double current_surface = 0.5 * cross.magnitude();
				if (i == 0){
					min_surface	= current_surface;
					max_surface = current_surface;
					min_edge = min<double>(u.magnitude(),v.magnitude());
					max_edge = max<double>(u.magnitude(),v.magnitude());
				}
				else{
					min_surface = min<double>(current_surface, min_surface);
					max_surface = max(current_surface, max_surface);
					min_edge = min<double>(min_edge, min(u.magnitude(),v.magnitude()));
					max_edge = max<double>(max_edge, max(u.magnitude(),v.magnitude()));
				}
				if ((current_surface < limit_surface) && (u.magnitude() < limit_edge) && (v.magnitude() < limit_edge)){
					vector<int> link;
					link.push_back(x);
					link.push_back(y);
					link.push_back(z);
					liens.push_back(link);				
				}
				i++;
			}
		}
		
		fichier.close();
		
		std::string outfilename = "model_surface_filtered.obj";
		
		ofstream fichier3(outfilename.c_str());
		if (fichier3){
			int n = points.size();
			for (int k=0; k<n; k++){
				vector3d goal = points[k];
				fichier3 << "v" << " " << goal.x << " " << goal.y << " " << goal.z << endl;
			}
			int m = liens.size();
			for (int l=0; l<m; l++){
				fichier3 << "f" << " " << liens[l][0] << " " << liens[l][1] << " " << liens[l][2] << endl;
			}
			cout << "Succesfully saved model_surface_fitered.obj" << endl;
			fichier3.close();
		}
		cout << "Surface limit was : " << limit_surface << endl;
		cout << "min_surface = " << min_surface << endl;
		cout << "max_surface = " << max_surface << endl;
		cout << "min_edge = " << min_edge << endl;
		cout << "max_edge = " << max_edge << endl;
		cout << i << " surfaces in" << endl;
		cout << liens.size() << " surfaces out" << endl;	
		cout << i - liens.size() << " faces were deleted" << endl;
	}
	
	else {cerr << "impossible d'ouvrir le fichier" << endl;}
	return(0);
}
