#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>
#include <bits/stdc++.h>

#include "vector.h"

#define PI 3.141592651

using namespace std;

typedef float f;

template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;
  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}


int main(int argc, char **argv){

	if (argc != 2) {
		cout << "usage : ./executable KeyframeTrajectory.txt" << endl;
	}
	
	double isovist_number = 16.0;

	string filetraj = argv[1];
	
	ifstream fichier2(filetraj.c_str(), ios::in);
	
	vector< vector3d > trajectory;

	double distance = 0;
		
	if(fichier2){
		while (fichier2){
			double time, x, y, z, q1, q2, q3, q4;
			
			fichier2 >> time >> x >> y >> z >> q1 >> q2 >> q3 >> q4;
			vector3d traj;
			traj.x = x;
			traj.y = 0;
			traj.z = z;
			if (trajectory.size() > 0){
				distance = distance + traj.distance(trajectory[trajectory.size() - 1]);
			}
			trajectory.push_back(traj);
		}
		fichier2.close();
	}
	
	double dist_limit = distance/isovist_number;
	double dist_count = 0;
	
	cout << "distance totale = " << distance << endl; 
	cout << "dist_limit = " << dist_limit << endl;
	
	cout << "isovist : 0" << endl;
	int last = 0;
	for(int i = 1; i< trajectory.size(); i++){
		dist_count = dist_count + trajectory[i].distance(trajectory[i-1]);
		if((dist_count - dist_limit > -0.05) || (dist_count >= dist_limit)){
			cout << "isovist : " << i << endl;
			cout << "distance from preceding = " << dist_count << endl;
			last = i;
			dist_count = 0;
		}
	}
	
	cout << "distance from " << last << " to " << trajectory.size() - 1 << " = " << trajectory[trajectory.size() - 1].distance(trajectory[last]) << endl;

	return 0;
}
