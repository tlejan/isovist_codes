#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>

#include "vector.h"

#define PI 3.14159265

using namespace std;

//class definition from https://gist.github.com/rishav007/accaf10c7aa06135d34ddf3919ebdb3b


typedef float f;


//**********************************************************************

template <typename T>
std::string to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;
  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}



//**********************************************************************

//Möller–Trumbore intersection algorithm

bool RayIntersectsTriangle(vector3d rayOrigin, 
                           vector3d rayVector, 
                           vector3d vertex0,
                           vector3d vertex1,
                           vector3d vertex2,
                           vector3d& outIntersectionPoint)
{
    const float EPSILON = 0.0000001;
    vector3d edge1, edge2, h, s, q;
    float a,b,u,v;
    edge1 = vertex1 - vertex0;
    edge2 = vertex2 - vertex0;
    h = rayVector.cross_product(edge2);
    a = edge1.dot_product(h);
    if (a > -EPSILON && a < EPSILON)
        return false;    // Le rayon est parallèle au triangle.

    b = 1.0/a;
    s = rayOrigin - vertex0;
    u = b * (s.dot_product(h));
    if (u < 0.0 || u > 1.0)
        return false;
    q = s.cross_product(edge1);
    v = b * rayVector.dot_product(q);
    if (v < 0.0 || u + v > 1.0)
        return false;

    // On calcule t pour savoir ou le point d'intersection se situe sur la ligne.
    float t = b * edge2.dot_product(q);
    if (t > EPSILON) // Intersection avec le rayon
    {
        outIntersectionPoint = rayOrigin + rayVector * t;
        return true;
    }
    else // On a bien une intersection de droite, mais pas de rayon.
        return false;
}


//**********************************************************************
 
int main(int argc, char **argv)
{
	
	
//IMPORTANT PARAMETERS

	double ray_length = 2.0; 
	double angle_pas = 1.0; //en degrés
	double scale = 6.1;
	int freq = 1; //fréquence des isovists conservés : 1 on sauvegarde tous les isovists ; 10 : on sauvegarde un isovist sur 10 etc...

//STARTING
	
	if (argc != 4){
		cerr << "Usage : ./isovist données/model.obj données/KeyFrameTrajectory.txt 2Dor3D" << endl;}
	
	
//LOADING MODEL.OBJ DATA		

	string filemodel = argv[1];
	
	ifstream fichier(filemodel.c_str(), ios::in);
	
	string isovist_shape = argv[3];
	
	vector< vector3d > points;
	vector< vector <int> > liens;
	bool point = true;
	
	if(fichier){
		while (fichier){
			double x, y, z;
			char u;
			fichier >> u >> x >> y >> z;
			
			point = (u == 'v');
			
			if (point){
				vector3d pt;
				pt.x = x;
				pt.y = y;
				pt.z = z;
				points.push_back(pt);
			}
			else{
				vector<int> link;
				link.push_back(x);
				link.push_back(y);
				link.push_back(z);
				liens.push_back(link);
			}
		}
		fichier.close();
	}
	
	else {cerr << "impossible d'ouvrir le fichier" << endl;}
	
	cout << points.size() << " data points were loaded" << endl;
	
	cout << liens.size() << " faces were loaded" << endl;


//LOADING TRAJECTORY
	
	string filetraj = argv[2];
	
	ifstream fichier2(filetraj.c_str(), ios::in);
	
	vector< vector3d > trajectory;
	
	if(fichier2){
		while (fichier2){
			double time, x, y, z, q1, q2, q3, q4;
			
			fichier2 >> time >> x >> y >> z >> q1 >> q2 >> q3 >> q4;
			vector3d traj;
			traj.x = x;
			traj.y = y;
			traj.z = z;
			trajectory.push_back(traj);
		}
		fichier2.close();
	}
	
	system("rm -r isovists");
	system("mkdir isovists");
	
	std::string command = "cp " + filemodel + " isovists";
	
	system(command.c_str());
	
	std::string outfilename = "isovists/trajectory.obj";
		
	ofstream fichier3(outfilename.c_str());
	if (fichier3){
		int n = trajectory.size();
		for (int k=0; k<n; k++){
			vector3d goal = trajectory[k]*scale;
			fichier3 << "v" << " " << goal.x << " " << 0 << " " << goal.z << endl;
		}
		for (int l=1; l<n-2; l++){
			fichier3 << "l" << " " << l << " " << l+1 << endl;
		}
		fichier3 << "l" << " " << n-1 << " " << 1 << endl;
		cout << "Succesfully saved trajectory.obj" << endl;
		fichier3.close();
	}
	
	
//RAY TRACING	
	
	/*
	trajectory.clear();
	vector3d pose;
	pose.x = 1.0;
	pose.y = 0 ;
	pose.z = 0.5;
	trajectory.push_back(pose);
	*/
	
	if (isovist_shape == "2D"){
		vector< vector3d > destinations;
	
		for (int i=0; i<trajectory.size(); i++){
			if(i%freq == 0){
				vector3d rayOrigin = trajectory[i];
				double angle = angle_pas;
				while (angle < 360.0){
					vector3d rayVector;
					rayVector.x = cos(angle * PI / 180.0);
					rayVector.y = 0;
					rayVector.z = sin(angle * PI / 180.0);
					vector3d dest = rayOrigin + rayVector*ray_length;
					for (int j=0; j<liens.size(); j++){
						vector3d vertex0 = points[liens[j][0] - 1];
						vector3d vertex1 = points[liens[j][1] - 1];
						vector3d vertex2 = points[liens[j][2] - 1];
						vector3d outIntersectionPoint;
						if (RayIntersectsTriangle(rayOrigin, rayVector, vertex0, vertex1, vertex2, outIntersectionPoint)){
							if (rayOrigin.distance(dest) > rayOrigin.distance(outIntersectionPoint)){
								dest = outIntersectionPoint;
		
							}
						}
					}
					destinations.push_back(dest*scale);
					angle += angle_pas;
				}
				std::string outfilename = "isovists/isovist_" + to_string(i) + ".obj";
				
				ofstream fichier(outfilename.c_str());
				if (fichier){
					int n = destinations.size();
					for (int k=0; k<n; k++){
						vector3d goal = destinations[k];
						fichier << "v" << " " << goal.x << " " << 0 << " " << goal.z << endl;
					}
					for (int l=1; l<n-2; l++){
						fichier << "l" << " " << l << " " << l+1 << endl;
					}
					fichier << "l" << " " << n-1 << " " << 1 << endl;
					cout << "Succesfully saved isovist " << i/freq << "/" << (trajectory.size() - 1)/freq << endl;
					fichier.close();
				}
				destinations.clear();
			}		
		}
	}
	
	if (isovist_shape == "3D"){
		
		
		
		vector< vector3d > destinations;
	
		for (int i=0; i<trajectory.size(); i++){
			if(i%freq == 0 && i>0){
				vector3d rayOrigin = trajectory[i];
				double angle1 = 0;
				while (angle1 < 360.0){
					cout << "3D isovist calcul : " << floor(angle1*100/360) << "%                " << '\r';
					vector3d rayVector;
					rayVector.x = cos(angle1 * PI / 180.0);
					rayVector.y = 0;
					rayVector.z = sin(angle1 * PI / 180.0);
					double angle2 = 0;
					vector3d u = rayVector.cross_product(vector3d(0, 1, 0));
					while (angle2 < 360.0){
						vector3d rotation1;
						vector3d rotation2;
						vector3d rotation3;
						vector3d rayVector2;
						double c = cos(angle2 * PI / 180.0);
						double s = sin(angle2 * PI / 180.0);
						rotation1.x = u.x * u.x * (1 - c) + c;
						rotation1.y = u.x * u.y * (1 - c) - u.z * s;
						rotation1.z = u.x * u.z * (1 - c) + u.y * s;
						rotation2.x = u.x * u.y * (1 - c) + u.z * s;
						rotation2.y = u.y * u.y * (1 - c) + c;
						rotation2.z = u.y * u.z * (1 - c) - u.x * s;
						rotation3.x = u.x * u.z * (1 - c) - u.y * s;
						rotation3.y = u.y * u.z * (1 - c) + u.x * s;
						rotation3.z = u.z * u.z * (1 - c) + c;
						rayVector2.x = rayVector.dot_product(rotation1);
						rayVector2.y = rayVector.dot_product(rotation2);
						rayVector2.z = rayVector.dot_product(rotation3);
						vector3d dest = rayOrigin + rayVector2*ray_length;
						for (int j=0; j<liens.size(); j++){
							vector3d vertex0 = points[liens[j][0] - 1];
							vector3d vertex1 = points[liens[j][1] - 1];
							vector3d vertex2 = points[liens[j][2] - 1];
							vector3d outIntersectionPoint;
							if (RayIntersectsTriangle(rayOrigin, rayVector2, vertex0, vertex1, vertex2, outIntersectionPoint)){
								if (rayOrigin.distance(dest) > rayOrigin.distance(outIntersectionPoint)){
									dest = outIntersectionPoint;
		
								}
							}
						}
						destinations.push_back(dest);
						angle2 += angle_pas;
					}
					angle1 += angle_pas;
				}
				std::string outfilename = "isovists/isovist_" + to_string(i/10) + ".obj";
				
				ofstream fichier(outfilename.c_str());
				if (fichier){
					int n = destinations.size();
					for (int k=0; k<n; k++){
						vector3d goal = destinations[k];
						fichier << "v" << " " << goal.x << " " << goal.y << " " << goal.z << endl;
					}
					
					for (int m=1; m < 360; m++){
						for (int l=m*360; l<m*360+358; l++){
							fichier << "l" << " " << l << " " << l+1 << endl;
						}
					}
					
					
					cout << "Succesfully saved isovist " << i/freq << "/" << (trajectory.size() - 1)/freq << endl;
					fichier.close();
				}
				destinations.clear();
			}		
		}
	}
	
	
	
	return(0);
}
